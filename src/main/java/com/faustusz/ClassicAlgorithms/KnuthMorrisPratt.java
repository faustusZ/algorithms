package com.faustusz.ClassicAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;

public class KnuthMorrisPratt {

    /*
    * 0. 问题介绍
    * 1. 最朴素的思路
    * 2. 既然已经匹配了前j个字符, 那么可以把这个已知的信息利用起来么 -- 部分匹配表(Partial Match Table)
    * 3. 如何计算部分匹配表
    * #. Reference:
    *
    * [1] 《阮一峰的网络日志》 http://www.ruanyifeng.com/blog/2013/05/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm.html
    *
    * [2] http://jakeboxer.com/blog/2009/12/13/the-knuth-morris-pratt-algorithm-in-my-own-words/
    *
    * [3] https://zh.wikipedia.org/wiki/%E5%85%8B%E5%8A%AA%E6%96%AF-%E8%8E%AB%E9%87%8C%E6%96%AF-%E6%99%AE%E6%8B%89%E7%89%B9%E7%AE%97%E6%B3%95
    *
    */

    @SuppressWarnings("WeakerAccess")
    public static int firstMatchSolve(String s, String pattern) {

        int[] partialMatchTable = getPartialMatchTable(pattern);

        int i = 0, j = 0;

        while (i < s.length() && j < pattern.length()) {
            if (s.charAt(i) == pattern.charAt(j)) {
                ++i;
                if (++j == pattern.length()) { // 终止条件, 当j == pattern.length时意味着已完全匹配
                    return i - j;
                }
            } else {
                if (j == 0) { // 边界条件, 当j == 0时, 即不存在上一个匹配的字符
                    ++i;
                } else {
                    /*
                    * 关键步骤: 当不匹配时, i不是朴素地自增1, 按照如下公式增加:
                    *     i要增加的数 = 已匹配的字符数 - 最后一个匹配字符在部分匹配表中对应的值
                    * 由此我们看出, 部分匹配表的重要性.
                    */
//                    i += j - partialMatchTable[j - 1];
                    /*
                    * 上面这行代码也是个大bug, 被Input = "mississippi", "issip"测出来了, i在自增的过程中已经比原来的i增加了j了,
                    * 所以不需要再+j, 相反应该-partialMatchTable[j - 1], j应该等于0. 同时我们又知道, 前partialMatchTable[j - 1]
                    * 个字符是相同的, 所以可以各自再加上partialMatchTable[j - 1]进行比较. 因此最终的结果就是i保持不变,
                    * j = partialMatchTable[j - 1]
                    */
                    j = partialMatchTable[j - 1];
                }
            }
        }

        return -1;
    }

    @SuppressWarnings("WeakerAccess")
    public static int[] allMatchSolve(String s, String pattern) {

        ArrayList<Integer> result = new ArrayList<>();

        int[] partialMatchTable = getPartialMatchTable(pattern);

        int i = 0;
        int j = 0;
        while (i < s.length() && j < pattern.length()) {
            if (s.charAt(i) == pattern.charAt(j)) {
                ++i;
                if (++j == pattern.length()) {
                    result.add(i - j);
                    j = 0;
                }
            } else {
                if (j == 0) {
                    ++i;
                } else {
                    j = partialMatchTable[j - 1];
                }
            }
        }

        int[] ret = new int[result.size()];
        for (int k = 0; k < result.size(); k++) {
            ret[k] = result.get(k);
        }
        return ret;
    }

    @SuppressWarnings("WeakerAccess")
    public static int[] getPartialMatchTable(String pattern) {

        /*
        * 所谓部分匹配表(Partial Match Table) pmt, 是指:
        *
        * 给定一个字符串String s, 其子串(包含本身)subString = s.substring(0, i+1)
        * (0 <= i < s.length)中, 前缀集(properPrefixes)和后缀集(properSuffixes)的最长共有元素(longestMatchElement)的长度,
        * 以"ABcdABd"为例:
        *
        * i = 0, subString = "A",
        * properPrefixes = [],
        * properSuffixes = [],
        * longestMatchElement = null,
        * pmt(0) = 0;
        *
        * i = 1, subString = "AB",
        * properPrefixes = ["A"],
        * properSuffixes = ["B"],
        * longestMatchElement = null
        * pmt(1) = 0;
        *
        * i = 2, subString = "ABc",
        * properPrefixes = ["A", "AB"],
        * properSuffixes = ["Bc", "c"],
        * longestMatchElement = null
        * pmt(2) = 0;
        *
        * i = 3, subString = "ABcd",
        * properPrefixes = ["A", "AB", "ABc"],
        * properSuffixes = ["Bcd", "cd", "d"],
        * longestMatchElement = null
        * pmt(3) = 0;
        *
        * i = 4, subString = "ABcdA",
        * properPrefixes = ["A", "AB", "ABc", "ABcd"],
        * properSuffixes = ["BcdA", "cdA", "dA", "A"],
        * longestMatchElement = "A"
        * pmt(4) = 1;
        *
        * i = 5, subString = "ABcdAB",
        * properPrefixes = ["A", "AB", "ABc", "ABcd", "ABcdA"],
        * properSuffixes = ["BcdAB", "cdAB", "dAB", "AB", "B"],
        * longestMatchElement = "AB"
        * pmt(5) = 2;
        *
        * i = 6, subString = "ABcdABd",
        * properPrefixes = ["A", "AB", "ABc", "ABcd", "ABcdA", "ABcdAB"],
        * properSuffixes = ["BcdABd", "cdABd", "dABd", "ABd", "Bd", "d"],
        * longestMatchElement = null
        * pmt(6) = 0;
        *
        * 下面的问题是, 如何将上述计算代码实现:
        * 我们知道, 共有元素的长度是相同的, 所以每次循环找共有元素的方法时间复杂度的上限是O(n), 千万不要properPrefixes和properSuffixes
        * 两个集合里的元素两两比较, 把时间复杂度提高到O(n^2), 伪代码如下:
        *
        * int[] pmt = new int[pattern.length()];
        * for(int i = 0; i < pattern.length(); i++) {
        *     result = 0;
        *     for (int j = 0; j < properPrefixes.length; j++) {
        *         if(properPrefixes[j].equal(properSuffixes[properPrefixes.length - 1- j])) {
        *             result = j + 1;
        *         }
        *     }
        *     pmt[i] = result;
        * }
        *
        * 下面, 我们对上述程序进行化简:
        * properPrefixes.length == i;
        * properPrefixes[j] = pattern.substring(0, j + 1)
        * properSuffixes[i - j - 1] = pattern.substring(i - j, i + 1)
        *
        * 于是得到如下伪代码:
        * int[] pmt = new int[pattern.length()];
        * int result;
        * for(int i = 0; i < pattern.length(); i++) {
        *     result = 0;
        *     for (int j = 0; j < i; j++) {
        *         if(pattern.substring(0, j + 1).equals(pattern.substring(i - j, i + 1))) {
        *             result = j + 1;
        *         }
        *     }
        *     pmt[i] = result;
        * }
        *
        * 上述计算虽然结果正确, 但是奈何总算法时间复杂度为O(n^2), 显然是不可取的.
        *
        * 继续观察可以发现, properPrefixes(i = m)和properPrefixes(i = m + 1)以及properSuffixes(i = m)和properSuffixes(i = m + 1)
        * 之间是有规律的. properPrefixes(i = m + 1) 比 properPrefixes(i = m) 多一个元素, 为pattern.substring(0, m + 1), 亦即
        * pattern.substring(0, m) + pattern.charAt(m), 而properSuffixes(i = m + 1)[k] = properSuffixes(i = m)[k] + pattern.charAt(m + 1)
        * 那么可以利用动态规划的思想, 把上一次运算的结果利用起来吗?
        *
        * 我们继续观察可以发现, 如果pmt[m + 1] == k, 那么必然有pmt[m] == k - 1, 证明:
        *
        * pmt[m + 1] == k ->
        * properPrefixes(i = m + 1)[k - 1] == properSuffixes(i = m + 1)[m + 1 - k] ->
        * pattern.substring(0, k) == pattern.substring(m - k + 2, m + 2) ->
        * pattern.substring(0, k - 1) == pattern.substring(m - k + 2, m + 1) ->
        * properPrefixes(i = m)[k - 1 - 1] == pattern.substring(m - (k - 2), m + 1) ->
        * properPrefixes(i = m)[k - 1 - 1] == properSuffixes(i = m)[m - (k - 2) - 1] ->
        * properPrefixes(i = m)[k - 1 - 1] == properSuffixes(i = m)[m - (k - 1)] ->
        * pmt[m] == k - 1
        *
        * 显然, 这是可以动态规划的.
        *
        * int[] pmt = new int[s.length()];
        * int j = 0;
        * for (int i = 1; i < s.length(); i++) {
        *     if (s.charAt(i) == s.charAt(j)) {
        *         pmt[i] = pmt[i - 1] + 1;
        *         j++;
        *     } else {
        *         pmt[i] = 0;
        *         j = 0;
        *     }
        * }
        * 这里的复杂度降为了O(n)
        *
        * 进一步化简上述代码:
        * int[] pmt = new int[s.length()];
        * int j = 0;
        * for (int i = 1; i < s.length(); i++) {
        *     if (s.charAt(i) == s.charAt(j)) {
        *         pmt[i] = ++j;
        *     } else {
        *         j = 0;
        *     }
        * }
        *
        */

        /*
        * 以下是首次提交时的代码, 我认为此代码即是完全正确的, 后来在一次使用Partial Match Table的计算(见
        * ShortestPalindrome.solvePlus()方法)中发现了此代码的缺陷:

        int[] lookUpTable = new int[pattern.length()];

        int i = 1, j = 0;

        for (; i < pattern.length(); i++) {
            if (pattern.charAt(i) == pattern.charAt(j)) {
                lookUpTable[i] = ++j;
            } else {
                j = 0;
            }
        }

        * 我们可以通过如下用例发现上述代码的fatal error:
        *     Input: "AACAAAC"
        * 那么, lookUpTable[4] = 2被算出来后, i = 5, j = 2, 必然有:
        *     pattern.charAt(i) == 'A' != pattern.charAt(j) == 'C'
        * 于是j重置为0, 于是:
        *     lookUpTable[5] = 0
        * 然而i = 5时的子串是:
        *     "AACAAA"
        * 此时lookUpTable[5] = 2
        * 也就是说, 循环体中的那个判断条件: pattern.charAt(i) == pattern.charAt(j) 不成立时, 不应该简单粗暴地将j置为0, 而应该向
        * 前回溯.
        */

        /*
        int[] lookUpTable = new int[pattern.length()];

        int i = 1, j = 0;

        for (; i < pattern.length(); i++) {
            while (j > 0 && pattern.charAt(i) != pattern.charAt(j)) {
                --j;
            }
            if (pattern.charAt(i) == pattern.charAt(j)) {
                lookUpTable[i] = ++j;
            }
        }
        *
        * 以上代码仍旧有缺陷, 可用Input = "ababb" 验出.
        *
        * 现在我们已经知道, 在pattern.charAt(i) == pattern.charAt(j)的处理了, 这借助了动态规划的思想, 上一次运行的结果+1就是本次
        * 运行的结果.
        *
        * 但在pattern.charAt(i) != pattern.charAt(j)时候的处理, 我就显得非常笨了. 第一次是直接将j置为0, 被"AACAAAC"测出问题, 第
        * 二次是把j置为j--, 被"ababb"测出了问题.
        *
        * 通过查看已有的正确代码, 我意识到问题出在了哪. 当pattern.charAt(i) != pattern.charAt(j)时, 我们其实知道
        * pattern.substring(0, j) == pattern.substring(i - j, i), 同时我们知道pattern.substring(0, j)的部分匹配值为pmt[j - 1],
        * 因此下一个需要和pattern.charAt(i)比较的, 既不是pattern.charAt(0)也不是pattern.charAt(--j), 而是pattern.charAt(pmt[j - 1]).
        */

        int[] lookUpTable = new int[pattern.length()];

        int i = 1, j = 0;

        for (; i < pattern.length(); i++) {
            while (j > 0 && pattern.charAt(i) != pattern.charAt(j)) {
                j = lookUpTable[j - 1];
            }
            if (pattern.charAt(i) == pattern.charAt(j)) {
                lookUpTable[i] = ++j;
            }
        }

        System.out.println(Arrays.toString(lookUpTable));

        return lookUpTable;
    }

    public static class Pro extends KnuthMorrisPratt {
        /*
        * 完成了上述KMP算法的推演之后, 我们会发现有如下两个规律:
        * (1) Partial Match Table 的第一项总是0
        * (2) 在solve方法中, 我们总是要考虑partialMatchTable[j - 1]中, j是否等于0的问题
        *
        * 那么有没有办法通过修改partialMatchTable的格式, 避免掉(2)中的问题呢? 即如下代码
        *
        * if (j == 0) {
        *     ++i;
        * } else {
        *     i += j;
        *     j = partialMatchTable[j - 1];
        * }
        *
        * 后来经过一番尝试, 发现j == 0时总是一个特殊情况, 无论如何都无法消除, 这又是边界条件的不可规避性. 计划遂放弃.
        */
    }
}
