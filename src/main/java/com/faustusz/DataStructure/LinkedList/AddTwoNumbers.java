package com.faustusz.DataStructure.LinkedList;

public class AddTwoNumbers {

    public ListNode solve(ListNode l1, ListNode l2) {
        int sum;
        int remainder;
        int carry = 0;
        ListNode currentNode1 = l1;
        ListNode currentNode2 = l2;

        int val1 = 0;
        val1 = currentNode1.val;
        currentNode1 = currentNode1.next;
        int val2 = 0;
        val2 = currentNode2.val;
        currentNode2 = currentNode2.next;
        sum = val1 + val2 + carry;
        remainder = sum % 10;
        carry = sum / 10;
        ListNode head = new ListNode(remainder);
        ListNode tail = head;

        while (currentNode1 != null || currentNode2 != null || carry > 0) {
            val1 = 0;
            if (null != currentNode1) {
                val1 = currentNode1.val;
                currentNode1 = currentNode1.next;
            }
            val2 = 0;
            if (null != currentNode2) {
                val2 = currentNode2.val;
                currentNode2 = currentNode2.next;
            }
            sum = val1 + val2 + carry;
            remainder = sum % 10;
            carry = sum / 10;
            tail.next = new ListNode(remainder);
            tail = tail.next;
        }
        return head;
    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
        }
    }
}
