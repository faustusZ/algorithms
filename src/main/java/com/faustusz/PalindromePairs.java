package com.faustusz;

import java.util.ArrayList;
import java.util.List;

public class PalindromePairs {

    public static List<List<Integer>> solve(String[] words) {

        List<List<Integer>> ret = new ArrayList<>();
        for (int i = 0; i < words.length - 1; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (palindrome(words[i]+words[j])) {
                    List<Integer> list = new ArrayList<>();
                    list.add(i);
                    list.add(j);
                    ret.add(list);
                }
                if (palindrome(words[j]+words[i])) {
                    List<Integer> list = new ArrayList<>();
                    list.add(j);
                    list.add(i);
                    ret.add(list);
                }
            }
        }
        return ret;
    }

    public static boolean palindrome(String s) {
        int i = 0;
        while (i < s.length() / 2) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                return false;
            }
            i++;
        }
        return true;
    }
}