package com.faustusz.DynamicPrograming;

@SuppressWarnings("WeakerAccess")
public class SubArrayProductLessThanK {
    /*
    * Description:
    *
    * You are given an array of positive integers 'nums'
    * Count and print the number of contiguous subArrays where the product of all the elements in the subArray is less
    * than 'k'.
    *
    * Example 1:
    *
    * Input: nums = [10, 5, 2, 6], k = 100
    * Output: 8
    * Explanation: The 8 subArrays that have product less than 100 are:
    *                   [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2 ,6].
    *              Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k = 100.
    *
    * Note:
    *
    * 0 < nums.length < = 50000.
    * 0 < nums[i] < 1000.
    * 0 <= k < 10^6.
    *
    * Link:
    *
    * LeetCode.cn 713th question.
    * https://leetcode-cn.com/problems/subarray-product-less-than-k/
    */

    public static int solve(int[] nums, int k) {
        /*
        * 分析: 题目中的限定条件"连续contiguous"是解决此问题的关键
        *       当我们试图遍历一个数组的全部"连续"子数组时, 我们可以采用如下方法:
        *           设连续子数组的最左边元素index为left, 最右边元素index为right, 则遍历方法可以为:
        *               for 0 <= right < nums.length:
        *                   for 0 <= left <= right:
        *       毫无疑问, 这样遍历的算法复杂度为O(n^2)
        *
        *       同时我们注意到另一个限定条件: 元素为positive, 因此他们的乘积有一定规律, 假设有临界条件:
        *           nums[left]*nums[left+1]*...*nums[right] < k
        *           nums[left-1]*nums[left]*...*nums[right] >= k 成立
        *       那么:
        *           nums[left-1]*nums[left]*...*nums[right]*nums[right+1] >= k 一定成立
        *       因此我们只需要比较:
        *                   nums[left]*...*nums[right]*nums[right+1] ? k
        *       就可以.
        *
        *       因此, for 0 <= right < nums.length 时, left不必每次从0取值, 右边元素index为right-1时的计算结果可以为右边元素index
        *       为right时的运算提供参考, 这正是动态规划的思想.
        *
        *       计算时间复杂度与空间复杂度:
        *           由于left不必每次从0取值, 因此最终的计算次数是, right从0加到n-1, left从0加到n-1, 二者不是做乘法而是做加法, 所
        *           以时间复杂度是O(n).
        *           所使用到的变量ret, left, product, right都定义一次, 因此空间复杂度为O(1)
        */
        if (k == 1) {
            /*
            * 边界条件的不可规避性
            * 通过尝试可以发现, 无论怎么设置初始状态(比如left=0, right=0或者left=-1, right=0), 循环条件以及循环语句体, 都无法得到
            * 一个可以在所有输入下都正确输出的程序, 因此针对边界条件, 只能通过特殊处理规避, 这就是边界条件的不可规避性(意即在没有特殊
            * 处理的情况下, 边界条件无法规避). 至于如何证明边界条件的不可规避性, 则限于我能力, 无法给出解答.
            */
            return 0;
        }
        int ret = 0;
        int left = 0;
        int right = 0;
        int product = 1;
        for (; right < nums.length; right++) {
            product *= nums[right];
            while (product >= k && left <= right) {
                product /= nums[left];
                left += 1;
            }
            ret += right - left + 1;
        }
        return ret;
        /*
        * 对于连续子区间+动态规划思想问题, 解题套路是一样的, 上面的解法还可以叫尺取法, 双指针法. 我也可以给它取名叫蚯蚓蠕动法
        * 因为可以用蚯蚓的蠕动形象地描述这个算法.
        */
    }
}
