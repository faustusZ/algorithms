package com.faustusz.DynamicPrograming;

public class LongestPalindromicSubString {
    /*
    * Description:
    *
    * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
    *
    * Example 1:
    *
    * Input: "babad"
    * Output: "bab"
    * Note: "aba" is also a valid answer.
    *
    * Example 2:
    *
    * Input: "cbbd"
    * Output: "bb"
    *
    * Link:
    *
    * 来源：力扣（LeetCode）
    * 链接：https://leetcode-cn.com/problems/longest-palindromic-substring
    * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
    */
    public static String solve(String s) {
        if (s.length() <= 1) { // 边界条件
            return s;
        }

        int length = s.length() * 10;
        int symLeft = s.length() * 5;
        int[] max = new int[2];

        while (symLeft / 5 >= max[1] - max[0] + 1 && symLeft > 5) {
            max = getMax(max, getNextSubString(getInit(symLeft), s));
            max = getMax(max, getNextSubString(getInit(length - symLeft), s));
            symLeft -= 5;
        }

        return s.substring(max[0], max[1] + 1);
    }

    private static int[] getNextSubString(int[] init, String s) {
        if (init[0] >= 0 && init[1] < s.length() && s.charAt(init[0]) == s.charAt(init[1])) {
            return getNextSubString(new int[] {init[0] - 1, init[1] + 1}, s);
        }
        if (init[1] - init[0] == 0) {
            return init;
        } else {
            return new int[] {init[0] + 1, init[1] - 1};
        }
    }

    private static int[] getInit(int sym) {
        int delta = sym % 10 + 5;
        return new int[] {(sym - delta) / 10, (sym + delta) / 10};
    }

    private static int[] getMax(int[] a, int[] b) {
        if (a[1] - a[0] >= b[1] - b[0]) {
            return a;
        } else {
            return b;
        }
    }
}
