package com.faustusz.DynamicPrograming;

import java.util.ArrayList;

public class NumbersWithSameConsecutiveDifferences {
    /*
    * Description:
    *
    * Return all non-negative integers of length N such that the absolute difference between every two consecutive digits
    * is K.
    * Note that every number in the answer must NOT have leading zero except for the number '0' itself. For example, '01'
    * has one leading zero and is invalid, but '0' is valid.
    * You may return the answer in any order.
    *
    * Example 1:
    *
    * Input: N = 3, K = 7
    * Output: [181, 292, 707, 818, 929]
    * Explanation: Note that '070' is not a valid number, because it has leading zero.
    *
    * Example 2:
    *
    * Input: N = 2, K = 1
    * Output: [10, 12, 21, 23, 32, 34, 43, 45, 54, 56, 65, 67, 76, 78, 87, 89, 98]
    *
    * Note:
    *
    * 1. 1 <= N <= 9
    * 2. 0 <= K <= 9
    *
    * Link:
    *
    * LeetCode.cn 967th question.
    * https://leetcode-cn.com/problems/numbers-with-same-consecutive-differences/
    */

    public int[] solve(int N, int K) {
        /*
        * 首先我对N == 1时的边界条件不是很确定, 通过运行LeetCode的测试用例才发现, N == 1时总是返回[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        * 虽然感觉毫无逻辑, 但是OK, 边界条件就这么定吧.
        *
        * 解题的思路往往是从一个特例开始的, 比如: (N = 3, K = 7) (N = 2, K = 1), 但是当我使用了特例 (N = 9, K = 1)才意识到问题的
        * 复杂性.
        *
        * 由此也发现了可以通过9个二叉树来视觉化这个问题, 以N = 3, K = 1为例:
        *     1           2           3...
        *   /  \        /  \
        *  0    2      1    3
        *  \   /\     /\   /\
        *  1  1 3    0 2  2 4
        *
        * 如果通过建立BinaryTreeNode类型实现一棵棵树, 再从书中通过中序遍历获得一个个数字, 光听起来就很麻烦.
        *
        * 想到本题标签是动态规划, 所谓动态规划则是前一次运算的结果可以作为下一次运算的参考, 那么如何将本题套路到这样的解法中去呢? 答案
        * 就是计算N = 3时的数字, 先计算N = 2时的数字, 在此基础上进行扩充, 计算N = 2时的数字则需要先计算N = 1时的数字...这听起来又很
        * 像是递归.
        *
        * 下一步再去想如何实现, 首先满足条件的数字的个数是不确定的, 在Java中需要用ArrayList链表实现. 其次, 上一次运行的结果需要保存
        * 给本次运行. 还有, 计算量非常大, 时间复杂度是O(N^2).
        *
        * 由此引出来我的一些疑惑:
        *     (1) 动态规划的概念和递归的概念是什么关系, 为什么考虑着动态规划突然就跑到了递归上去?
        *     (2) 自顶向下, DFS(Depth-First-Search)这两个概念和递归的关系又是什么?
        *     (3) 算上上次探讨动态规划的时候, 得到了"尺取法", "双指针法"两个概念, 我已经听到了太多算法的概念了, 他们都有什么关系?
        *
        */
        if (N == 1) { // 边界条件
            return new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        }

        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            result.add(i);
        }

        for (int i = 2; i <= N; i++) {
            result = addOneDigit(K, result);
        }

        int[] ret = new int[result.size()];
        for (int i = 0; i < result.size(); i++) {
            ret[i] = result.get(i);
        }
        return ret;
    }

    private ArrayList<Integer> addOneDigit(int K, ArrayList<Integer> last) {
        ArrayList<Integer> ret = new ArrayList<>();
        if (K == 0) { // 边界条件
            /*
            * 另外一个需要注意的边界条件是K = 0, 在后面的验证中会发现, 最初的代码会有如下bug:
            *     Input: N = 2, K = 0
            *     Output: [11, 11, 22, 22, 33, 33, 44, 44, 55, 55, 66, 66, 77, 77, 88, 88, 99, 99]
            *     Expected: [11, 22, 33, 44, 55, 66, 77, 88, 99]
            * 这是因为原初代码对于 num + 0 和 num - 0 是作为两个值对待的.
            */
            for (int num : last) {
                ret.add(num * 10 + num % 10);
            }
            return ret;
        }
        for (int num : last) {
            int remainder = num % 10;
            int menus = remainder - K;
            int add = remainder + K;
            if (menus >= 0) {
                ret.add(num * 10 + menus);
            }
            if (add < 10) {
                ret.add(num * 10 + add);
            }
        }
        return ret;
    }
}
