package com.faustusz.ClassicAlgorithms;

import org.junit.Test;

import static org.junit.Assert.*;

public class KnuthMorrisPrattTest {

    @Test
    public void firstMatchSolve() {
    }

    @Test
    public void allMatchSolve() {
    }

    @Test
    public void getPartialMatchTable() {
        /*
        * 测试用例:
        *
        * Input: "ABCDABD"
        * Output: [0, 0, 0, 0, 1, 2, 0]
        *
        * Input: "aaaaa"
        * Output: [0, 1, 2, 3, 4]
        *
        * Input: "AACAAAC"
        * Output: [0, 1, 0, 1, 2, 2, 3]
        *
        * Input: "ababbbabbaba#*?ababbabbbaba"
        * Output: []
        * */
        KnuthMorrisPratt.getPartialMatchTable("AAAAAAA");
        KnuthMorrisPratt.getPartialMatchTable("ABCDABD");
        KnuthMorrisPratt.getPartialMatchTable("AACAAAC");
        KnuthMorrisPratt.getPartialMatchTable("ababb");
        KnuthMorrisPratt.getPartialMatchTable("ababbbabbaba#*?ababbabbbaba");
    }
}