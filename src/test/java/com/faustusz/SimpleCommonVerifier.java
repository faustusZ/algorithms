package com.faustusz;

import com.faustusz.ClassicAlgorithms.KnuthMorrisPratt;
import com.faustusz.DynamicPrograming.LongestPalindromicSubString;

import java.util.Arrays;

public class SimpleCommonVerifier {

    public static void main(String[] args) {

        // KnuthMorrisPratt
        System.out.println(KnuthMorrisPratt.firstMatchSolve("ABC ABCDAB ABCDABCDABDE", "ABCDABD"));
        System.out.println(KnuthMorrisPratt.firstMatchSolve("ABC ABCDAB ABCDABCDABDE", "ABCDABDEABF"));
        System.out.println(KnuthMorrisPratt.firstMatchSolve("ABCD", "ABC"));
        System.out.println(KnuthMorrisPratt.firstMatchSolve("Puff a magical dragon, lived by the sea.", "dragon"));
        System.out.println(Arrays.toString(KnuthMorrisPratt.allMatchSolve("Does't mean I'm lonely when I'm alone.", "lone")));
        System.out.println(KnuthMorrisPratt.firstMatchSolve("Does't mean I'm lonely when I'm alone.", "aaaaa"));
        System.out.println(KnuthMorrisPratt.firstMatchSolve("mississippi", "issip"));

        // LongestPalindromicSubString
//        System.out.println(LongestPalindromicSubString.solve("cbbd"));
    }
}
