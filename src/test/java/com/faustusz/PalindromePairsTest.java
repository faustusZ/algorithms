package com.faustusz;

import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromePairsTest {

    @Test
    public void solve() {
    }

    @Test
    public void palindrome() {
        assertTrue(PalindromePairs.palindrome("aba"));
        assertTrue(PalindromePairs.palindrome("sslss"));
        assertTrue(PalindromePairs.palindrome("abba"));
    }
}