package com.faustusz.DynamicPrograming;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubArrayProductLessThanKTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void solve() {

        int[] nums1 = {10};
        assertEquals(1, SubArrayProductLessThanK.solve(nums1, 100));

        assertEquals(0, SubArrayProductLessThanK.solve(nums1, 10));

        int[] nums2 = {10, 5, 2, 6};
        assertEquals(8, SubArrayProductLessThanK.solve(nums2, 100));

        int[] nums3 = {1};
        assertEquals(0, SubArrayProductLessThanK.solve(nums3, 1));
    }
}